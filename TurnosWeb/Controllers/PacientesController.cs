﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TurnosWeb.Models;
using TurnosWeb.Servicios.ViewModels;

namespace TurnosWeb.Controllers
{
    public class PacientesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Pacientes
        public ActionResult Index()
        {
            
            return View(db.PacienteVMs.ToList());
        }

        // GET: Pacientes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacienteVM pacienteVM = db.PacienteVMs.Find(id);
            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // GET: Pacientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pacientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Apellido,Telefono,Celular,DNI,Mail,Direccion,FechaNacimiento,EstadoCivil,GrupoSanguineo,Sexo")] PacienteVM pacienteVM)
        {
            if (ModelState.IsValid)
            {
                pacienteVM.Id = Guid.NewGuid();
                db.PacienteVMs.Add(pacienteVM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pacienteVM);
        }

        // GET: Pacientes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacienteVM pacienteVM = db.PacienteVMs.Find(id);
            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // POST: Pacientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Apellido,Telefono,Celular,DNI,Mail,Direccion,FechaNacimiento,EstadoCivil,GrupoSanguineo,Sexo")] PacienteVM pacienteVM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pacienteVM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pacienteVM);
        }

        // GET: Pacientes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacienteVM pacienteVM = db.PacienteVMs.Find(id);
            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            PacienteVM pacienteVM = db.PacienteVMs.Find(id);
            db.PacienteVMs.Remove(pacienteVM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
