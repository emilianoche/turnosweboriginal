﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models;
using TurnosWeb.Models.Contexto;
using TurnosWeb.Servicios.ViewModels;

namespace TurnosWeb.Servicios
{
    public class PacienteServicio
    {
        public void Insertar(PacienteVM persona)
        {
            try
            {

                using (var _db=new ApplicationDbContext())
                {
                    var paciente = new Models.Paciente()
                    {
                       
                        Apellido = persona.Apellido,
                        Nombre = persona.Nombre,
                        Celular = persona.Celular,
                        Email = persona.Mail,
                        Sexo = persona.Sexo,
                        Telefono = persona.Telefono,
                        GrupoSanguineo = persona.GrupoSanguineo,
                        Dni = persona.DNI,
                        FechaNacimiento = persona.FechaNacimiento,
                        EstaBorrado = false,
                        EstadoCivil = persona.EstadoCivil,
                        Direccion = persona.Direccion
                    };
                    _db.Entry(paciente).State=System.Data.Entity.EntityState.Added;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        public List<PacienteVM> ObtenerPacientes()
        {
            try
            {
                using (var _db=new ApplicationDbContext())
                {
                    return _db.Pacientes.Select(x => new PacienteVM()
                    {
                        Apellido=x.Apellido,
                        Nombre=x.Nombre,
                       // Usuarioid=x.UsuarioId,
                        Celular=x.Celular,
                        Direccion=x.Direccion,
                        DNI=x.Dni,
                        EstadoCivil=x.EstadoCivil,
                        FechaNacimiento=x.FechaNacimiento,
                        GrupoSanguineo=x.GrupoSanguineo,
                        Id=x.Id,
                        Mail=x.Email,
                        Sexo=x.Sexo,
                        Telefono=x.Telefono
                         

                    }).ToList();
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
            
    }
}
