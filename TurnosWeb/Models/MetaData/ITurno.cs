﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Models.MetaData
{
    public interface ITurno
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "La {0} es obligatoria")]
        [DataType(DataType.Date)]
        DateTime Fecha { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="La {0} es obligatoria")]
        [DataType(DataType.Time)]
        TimeSpan HoraTurno { get; set; }
        [Display(Name ="@Motivo de la Consulta")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es obligatorio")]
        [DataType(DataType.MultilineText)]
        string MotivoConsulta { get; set; }
        [Display(Name ="@Urgente")]
        bool EsUrgente { get; set; }
        [Display(Name = "@Es Primera Vez")]
        bool EsPrimeraVez { get; set; }
        [DataType(DataType.Currency)]
        [Required(AllowEmptyStrings =false, ErrorMessage ="El {0} es obligatorio")]
        [Display(Name = "@Importe Consulta")]
        decimal Monto { get; set; }

        [EnumDataType(typeof(EstadoTurno))]
        EstadoTurno EstadoTurno { get; set; }
    }
}
